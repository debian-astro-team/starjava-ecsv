Source: starjava-ecsv
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               debhelper-compat (= 13),
               dh-exec,
               javahelper
Build-Depends-Indep: ant-optional,
                     default-jdk,
                     default-jdk-doc,
                     junit,
                     libandroid-json-org-java,
                     libyaml-snake-java,
                     starlink-table-java (>= 4.1.3~),
                     starlink-util-java (>= 1.0+2024.08.06~)
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/starjava-ecsv
Vcs-Git: https://salsa.debian.org/debian-astro-team/starjava-ecsv.git
Homepage: https://github.com/Starlink/starjava/tree/master/ecsv

Package: starlink-ecsv-java
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         starlink-table-java (>= 4.1.3~),
         starlink-util-java (>= 1.0+2024.08.06~)
Description: Parser for the metadata and data of an ECSV file
 The Enhanced Character Separated Values format was developed within
 the Astropy project and is described at APE6. It is composed of a
 YAML header followed by a CSV-like body, and is intended to be a
 human-readable and maybe even human-writable format with rich
 metadata. Most of the useful per-column and per-table metadata is
 preserved when de/serializing to this format. The version supported
 by this reader is currently ECSV 1.0.

Package: starlink-ecsv-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Recommends: ${java:Recommends}
Description: Parser for the metadata and data of an ECSV file (documentation)
 The Enhanced Character Separated Values format was developed within
 the Astropy project and is described at APE6. It is composed of a
 YAML header followed by a CSV-like body, and is intended to be a
 human-readable and maybe even human-writable format with rich
 metadata. Most of the useful per-column and per-table metadata is
 preserved when de/serializing to this format. The version supported
 by this reader is currently ECSV 1.0.
 .
 This package contains the JavaDoc documentation of the package.
